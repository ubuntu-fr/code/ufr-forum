-- MariaDB dump 10.19  Distrib 10.7.4-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: flarum
-- ------------------------------------------------------
-- Server version	10.7.4-MariaDB-1:10.7.4+maria~focal

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `flarum_access_tokens`
--

DROP TABLE IF EXISTS `flarum_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flarum_access_tokens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `token` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `last_activity_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_user_agent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `flarum_access_tokens_token_unique` (`token`),
  KEY `flarum_access_tokens_user_id_foreign` (`user_id`),
  KEY `flarum_access_tokens_type_index` (`type`),
  CONSTRAINT `flarum_access_tokens_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `flarum_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flarum_access_tokens`
--

LOCK TABLES `flarum_access_tokens` WRITE;
/*!40000 ALTER TABLE `flarum_access_tokens` DISABLE KEYS */;
INSERT INTO `flarum_access_tokens` VALUES
(16,'ceusrDwv9SX4GZuQbe4kuEEZgYVETXc5tSgB4x4g',1,'2022-08-02 08:50:20','2022-08-02 08:46:58','session',NULL,'172.18.0.2','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.134 Safari/537.36'),
(17,'uvkQeda1OPR7xYMhwBrUh484NYM8dDDgmUA7VTgA',1,'2022-08-02 18:51:25','2022-08-02 18:51:25','session',NULL,'172.18.0.2','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.134 Safari/537.36'),
(18,'Bfkxb0yhXXbM18ld2Gs3FJp8wLWar2K3nH4e5ath',1,'2022-08-11 17:25:57','2022-08-11 17:25:57','session',NULL,'172.18.0.2','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.5112.79 Safari/537.36');
/*!40000 ALTER TABLE `flarum_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flarum_api_keys`
--

DROP TABLE IF EXISTS `flarum_api_keys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flarum_api_keys` (
  `key` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `allowed_ips` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `last_activity_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `flarum_api_keys_key_unique` (`key`),
  KEY `flarum_api_keys_user_id_foreign` (`user_id`),
  CONSTRAINT `flarum_api_keys_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `flarum_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flarum_api_keys`
--

LOCK TABLES `flarum_api_keys` WRITE;
/*!40000 ALTER TABLE `flarum_api_keys` DISABLE KEYS */;
/*!40000 ALTER TABLE `flarum_api_keys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flarum_banned_ips`
--

DROP TABLE IF EXISTS `flarum_banned_ips`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flarum_banned_ips` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `creator_id` int(10) unsigned NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reason` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `flarum_banned_ips_address_unique` (`address`),
  KEY `flarum_banned_ips_user_id_foreign` (`user_id`),
  CONSTRAINT `flarum_banned_ips_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `flarum_users` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flarum_banned_ips`
--

LOCK TABLES `flarum_banned_ips` WRITE;
/*!40000 ALTER TABLE `flarum_banned_ips` DISABLE KEYS */;
/*!40000 ALTER TABLE `flarum_banned_ips` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flarum_discussion_tag`
--

DROP TABLE IF EXISTS `flarum_discussion_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flarum_discussion_tag` (
  `discussion_id` int(10) unsigned NOT NULL,
  `tag_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`discussion_id`,`tag_id`),
  KEY `flarum_discussion_tag_tag_id_foreign` (`tag_id`),
  CONSTRAINT `flarum_discussion_tag_discussion_id_foreign` FOREIGN KEY (`discussion_id`) REFERENCES `flarum_discussions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `flarum_discussion_tag_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `flarum_tags` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flarum_discussion_tag`
--

LOCK TABLES `flarum_discussion_tag` WRITE;
/*!40000 ALTER TABLE `flarum_discussion_tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `flarum_discussion_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flarum_discussion_user`
--

DROP TABLE IF EXISTS `flarum_discussion_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flarum_discussion_user` (
  `user_id` int(10) unsigned NOT NULL,
  `discussion_id` int(10) unsigned NOT NULL,
  `last_read_at` datetime DEFAULT NULL,
  `last_read_post_number` int(10) unsigned DEFAULT NULL,
  `subscription` enum('follow','ignore') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`user_id`,`discussion_id`),
  KEY `flarum_discussion_user_discussion_id_foreign` (`discussion_id`),
  CONSTRAINT `flarum_discussion_user_discussion_id_foreign` FOREIGN KEY (`discussion_id`) REFERENCES `flarum_discussions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `flarum_discussion_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `flarum_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flarum_discussion_user`
--

LOCK TABLES `flarum_discussion_user` WRITE;
/*!40000 ALTER TABLE `flarum_discussion_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `flarum_discussion_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flarum_discussion_views`
--

DROP TABLE IF EXISTS `flarum_discussion_views`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flarum_discussion_views` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `discussion_id` int(10) unsigned NOT NULL,
  `ip` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `visited_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `flarum_discussion_views_discussion_id_foreign` (`discussion_id`),
  KEY `flarum_discussion_views_user_id_foreign` (`user_id`),
  CONSTRAINT `flarum_discussion_views_discussion_id_foreign` FOREIGN KEY (`discussion_id`) REFERENCES `flarum_discussions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `flarum_discussion_views_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `flarum_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flarum_discussion_views`
--

LOCK TABLES `flarum_discussion_views` WRITE;
/*!40000 ALTER TABLE `flarum_discussion_views` DISABLE KEYS */;
/*!40000 ALTER TABLE `flarum_discussion_views` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flarum_discussions`
--

DROP TABLE IF EXISTS `flarum_discussions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flarum_discussions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_count` int(11) NOT NULL DEFAULT 1,
  `participant_count` int(10) unsigned NOT NULL DEFAULT 0,
  `post_number_index` int(10) unsigned NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `first_post_id` int(10) unsigned DEFAULT NULL,
  `last_posted_at` datetime DEFAULT NULL,
  `last_posted_user_id` int(10) unsigned DEFAULT NULL,
  `last_post_id` int(10) unsigned DEFAULT NULL,
  `last_post_number` int(10) unsigned DEFAULT NULL,
  `hidden_at` datetime DEFAULT NULL,
  `hidden_user_id` int(10) unsigned DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_private` tinyint(1) NOT NULL DEFAULT 0,
  `is_approved` tinyint(1) NOT NULL DEFAULT 1,
  `is_locked` tinyint(1) NOT NULL DEFAULT 0,
  `is_sticky` tinyint(1) NOT NULL DEFAULT 0,
  `view_count` int(11) NOT NULL DEFAULT 0,
  `best_answer_post_id` int(10) unsigned DEFAULT NULL,
  `best_answer_user_id` int(10) unsigned DEFAULT NULL,
  `best_answer_notified` tinyint(1) NOT NULL,
  `best_answer_set_at` datetime DEFAULT NULL,
  `votes` int(11) NOT NULL,
  `hotness` double(10,4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `flarum_discussions_hidden_user_id_foreign` (`hidden_user_id`),
  KEY `flarum_discussions_first_post_id_foreign` (`first_post_id`),
  KEY `flarum_discussions_last_post_id_foreign` (`last_post_id`),
  KEY `flarum_discussions_last_posted_at_index` (`last_posted_at`),
  KEY `flarum_discussions_last_posted_user_id_index` (`last_posted_user_id`),
  KEY `flarum_discussions_created_at_index` (`created_at`),
  KEY `flarum_discussions_user_id_index` (`user_id`),
  KEY `flarum_discussions_comment_count_index` (`comment_count`),
  KEY `flarum_discussions_participant_count_index` (`participant_count`),
  KEY `flarum_discussions_hidden_at_index` (`hidden_at`),
  KEY `flarum_discussions_is_locked_index` (`is_locked`),
  KEY `flarum_discussions_is_sticky_created_at_index` (`is_sticky`,`created_at`),
  KEY `flarum_discussions_is_sticky_last_posted_at_index` (`is_sticky`,`last_posted_at`),
  KEY `flarum_discussions_best_answer_post_id_foreign` (`best_answer_post_id`),
  KEY `flarum_discussions_best_answer_user_id_foreign` (`best_answer_user_id`),
  KEY `flarum_discussions_best_answer_set_at_index` (`best_answer_set_at`),
  KEY `flarum_discussions_votes_index` (`votes`),
  FULLTEXT KEY `title` (`title`),
  CONSTRAINT `flarum_discussions_best_answer_post_id_foreign` FOREIGN KEY (`best_answer_post_id`) REFERENCES `flarum_posts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `flarum_discussions_best_answer_user_id_foreign` FOREIGN KEY (`best_answer_user_id`) REFERENCES `flarum_users` (`id`) ON DELETE SET NULL,
  CONSTRAINT `flarum_discussions_first_post_id_foreign` FOREIGN KEY (`first_post_id`) REFERENCES `flarum_posts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `flarum_discussions_hidden_user_id_foreign` FOREIGN KEY (`hidden_user_id`) REFERENCES `flarum_users` (`id`) ON DELETE SET NULL,
  CONSTRAINT `flarum_discussions_last_post_id_foreign` FOREIGN KEY (`last_post_id`) REFERENCES `flarum_posts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `flarum_discussions_last_posted_user_id_foreign` FOREIGN KEY (`last_posted_user_id`) REFERENCES `flarum_users` (`id`) ON DELETE SET NULL,
  CONSTRAINT `flarum_discussions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `flarum_users` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flarum_discussions`
--

LOCK TABLES `flarum_discussions` WRITE;
/*!40000 ALTER TABLE `flarum_discussions` DISABLE KEYS */;
/*!40000 ALTER TABLE `flarum_discussions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flarum_drafts`
--

DROP TABLE IF EXISTS `flarum_drafts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flarum_drafts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `relationships` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_at` datetime NOT NULL,
  `scheduled_for` datetime DEFAULT NULL,
  `scheduled_validation_error` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extra` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `flarum_drafts_user_id_foreign` (`user_id`),
  CONSTRAINT `flarum_drafts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `flarum_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flarum_drafts`
--

LOCK TABLES `flarum_drafts` WRITE;
/*!40000 ALTER TABLE `flarum_drafts` DISABLE KEYS */;
/*!40000 ALTER TABLE `flarum_drafts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flarum_email_tokens`
--

DROP TABLE IF EXISTS `flarum_email_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flarum_email_tokens` (
  `token` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`token`),
  KEY `flarum_email_tokens_user_id_foreign` (`user_id`),
  CONSTRAINT `flarum_email_tokens_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `flarum_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flarum_email_tokens`
--

LOCK TABLES `flarum_email_tokens` WRITE;
/*!40000 ALTER TABLE `flarum_email_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `flarum_email_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flarum_flags`
--

DROP TABLE IF EXISTS `flarum_flags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flarum_flags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(10) unsigned NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `reason` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reason_detail` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `flarum_flags_post_id_foreign` (`post_id`),
  KEY `flarum_flags_user_id_foreign` (`user_id`),
  KEY `flarum_flags_created_at_index` (`created_at`),
  CONSTRAINT `flarum_flags_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `flarum_posts` (`id`) ON DELETE CASCADE,
  CONSTRAINT `flarum_flags_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `flarum_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flarum_flags`
--

LOCK TABLES `flarum_flags` WRITE;
/*!40000 ALTER TABLE `flarum_flags` DISABLE KEYS */;
/*!40000 ALTER TABLE `flarum_flags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flarum_fof_merge_discussions_redirections`
--

DROP TABLE IF EXISTS `flarum_fof_merge_discussions_redirections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flarum_fof_merge_discussions_redirections` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `request_discussion_id` bigint(20) unsigned NOT NULL,
  `to_discussion_id` bigint(20) unsigned NOT NULL,
  `http_code` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flarum_fof_merge_discussions_redirections`
--

LOCK TABLES `flarum_fof_merge_discussions_redirections` WRITE;
/*!40000 ALTER TABLE `flarum_fof_merge_discussions_redirections` DISABLE KEYS */;
/*!40000 ALTER TABLE `flarum_fof_merge_discussions_redirections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flarum_fof_upload_downloads`
--

DROP TABLE IF EXISTS `flarum_fof_upload_downloads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flarum_fof_upload_downloads` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `file_id` int(10) unsigned NOT NULL,
  `discussion_id` int(10) unsigned DEFAULT NULL,
  `post_id` int(10) unsigned DEFAULT NULL,
  `actor_id` int(10) unsigned DEFAULT NULL,
  `downloaded_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `flarum_fof_upload_downloads_file_id_foreign` (`file_id`),
  KEY `flarum_fof_upload_downloads_discussion_id_foreign` (`discussion_id`),
  KEY `flarum_fof_upload_downloads_actor_id_foreign` (`actor_id`),
  KEY `flarum_fof_upload_downloads_post_id_foreign` (`post_id`),
  CONSTRAINT `flarum_fof_upload_downloads_actor_id_foreign` FOREIGN KEY (`actor_id`) REFERENCES `flarum_users` (`id`) ON DELETE SET NULL,
  CONSTRAINT `flarum_fof_upload_downloads_discussion_id_foreign` FOREIGN KEY (`discussion_id`) REFERENCES `flarum_discussions` (`id`) ON DELETE SET NULL,
  CONSTRAINT `flarum_fof_upload_downloads_file_id_foreign` FOREIGN KEY (`file_id`) REFERENCES `flarum_fof_upload_files` (`id`) ON DELETE CASCADE,
  CONSTRAINT `flarum_fof_upload_downloads_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `flarum_posts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flarum_fof_upload_downloads`
--

LOCK TABLES `flarum_fof_upload_downloads` WRITE;
/*!40000 ALTER TABLE `flarum_fof_upload_downloads` DISABLE KEYS */;
/*!40000 ALTER TABLE `flarum_fof_upload_downloads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flarum_fof_upload_files`
--

DROP TABLE IF EXISTS `flarum_fof_upload_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flarum_fof_upload_files` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `actor_id` int(10) unsigned DEFAULT NULL,
  `discussion_id` int(10) unsigned DEFAULT NULL,
  `post_id` int(10) unsigned DEFAULT NULL,
  `base_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` int(11) NOT NULL,
  `upload_method` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `remote_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tag` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hide_from_media_manager` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `flarum_fof_upload_files_actor_id_hide_from_media_manager_index` (`actor_id`,`hide_from_media_manager`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flarum_fof_upload_files`
--

LOCK TABLES `flarum_fof_upload_files` WRITE;
/*!40000 ALTER TABLE `flarum_fof_upload_files` DISABLE KEYS */;
/*!40000 ALTER TABLE `flarum_fof_upload_files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flarum_group_permission`
--

DROP TABLE IF EXISTS `flarum_group_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flarum_group_permission` (
  `group_id` int(10) unsigned NOT NULL,
  `permission` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`group_id`,`permission`),
  CONSTRAINT `flarum_group_permission_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `flarum_groups` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flarum_group_permission`
--

LOCK TABLES `flarum_group_permission` WRITE;
/*!40000 ALTER TABLE `flarum_group_permission` DISABLE KEYS */;
INSERT INTO `flarum_group_permission` VALUES
(2,'fof-user-bio.view'),
(2,'fof.gamification.viewRankingPage'),
(2,'fof.user-directory.view'),
(2,'viewForum'),
(3,'discussion.downvote_notifications'),
(3,'discussion.flagPosts'),
(3,'discussion.likePosts'),
(3,'discussion.reply'),
(3,'discussion.replyWithoutApproval'),
(3,'discussion.selectBestAnswerOwnDiscussion'),
(3,'discussion.startWithoutApproval'),
(3,'discussion.upvote_notifications'),
(3,'discussion.votePosts'),
(3,'fof-user-bio.editOwn'),
(3,'searchUsers'),
(3,'startDiscussion'),
(4,'bypassTagCounts'),
(4,'discussion.approvePosts'),
(4,'discussion.editPosts'),
(4,'discussion.hide'),
(4,'discussion.hidePosts'),
(4,'discussion.lock'),
(4,'discussion.merge'),
(4,'discussion.rename'),
(4,'discussion.split'),
(4,'discussion.sticky'),
(4,'discussion.tag'),
(4,'discussion.viewFlags'),
(4,'discussion.viewIpsPosts'),
(4,'fof-upload.deleteUserUploads'),
(4,'fof-upload.viewUserUploads'),
(4,'fof-user-bio.editAny'),
(4,'user.createModeratorNotes'),
(4,'user.deleteModeratorNotes'),
(4,'user.editOwnNickname'),
(4,'user.manageWarnings'),
(4,'user.saveDrafts'),
(4,'user.scheduleDrafts'),
(4,'user.spamblock'),
(4,'user.suspend'),
(4,'user.viewLastSeenAt'),
(4,'user.viewModeratorNotes'),
(4,'user.viewWarnings');
/*!40000 ALTER TABLE `flarum_group_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flarum_group_user`
--

DROP TABLE IF EXISTS `flarum_group_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flarum_group_user` (
  `user_id` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`group_id`),
  KEY `flarum_group_user_group_id_foreign` (`group_id`),
  CONSTRAINT `flarum_group_user_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `flarum_groups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `flarum_group_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `flarum_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flarum_group_user`
--

LOCK TABLES `flarum_group_user` WRITE;
/*!40000 ALTER TABLE `flarum_group_user` DISABLE KEYS */;
INSERT INTO `flarum_group_user` VALUES
(1,1),
(2,5),
(3,6),
(5,4);
/*!40000 ALTER TABLE `flarum_group_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flarum_groups`
--

DROP TABLE IF EXISTS `flarum_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flarum_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name_singular` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_plural` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_hidden` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flarum_groups`
--

LOCK TABLES `flarum_groups` WRITE;
/*!40000 ALTER TABLE `flarum_groups` DISABLE KEYS */;
INSERT INTO `flarum_groups` VALUES
(1,'Admin','Admins','#B72A2A','fas fa-wrench',0),
(2,'Guest','Guests',NULL,NULL,0),
(3,'Member','Members',NULL,NULL,0),
(4,'Mod','Mods','#80349E','fas fa-bolt',0),
(5,'AdminWiki','AdminWiki','#ce4833','fas fa-pen',0),
(6,'ExpertWiki','ExpertWiki','#f3a000','fas fa-pen',0);
/*!40000 ALTER TABLE `flarum_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flarum_links`
--

DROP TABLE IF EXISTS `flarum_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flarum_links` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` int(11) DEFAULT NULL,
  `is_internal` tinyint(1) NOT NULL DEFAULT 0,
  `is_newtab` tinyint(1) NOT NULL DEFAULT 0,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `icon` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `visibility` enum('everyone','members','guests') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'everyone',
  PRIMARY KEY (`id`),
  KEY `flarum_links_parent_id_foreign` (`parent_id`),
  KEY `flarum_links_visibility_index` (`visibility`),
  CONSTRAINT `flarum_links_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `flarum_links` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flarum_links`
--

LOCK TABLES `flarum_links` WRITE;
/*!40000 ALTER TABLE `flarum_links` DISABLE KEYS */;
INSERT INTO `flarum_links` VALUES
(1,'Guide','https://doc.ubuntu-fr.org/tutoriel/howto_forum',NULL,0,1,NULL,'fas fa-question-circle','everyone'),
(2,'Règles','http://ufr-forum.localhost/p/1-regles',NULL,1,0,NULL,'fas fa-list-ul','everyone');
/*!40000 ALTER TABLE `flarum_links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flarum_login_providers`
--

DROP TABLE IF EXISTS `flarum_login_providers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flarum_login_providers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `provider` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `identifier` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `last_login_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `flarum_login_providers_provider_identifier_unique` (`provider`,`identifier`),
  KEY `flarum_login_providers_user_id_foreign` (`user_id`),
  CONSTRAINT `flarum_login_providers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `flarum_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flarum_login_providers`
--

LOCK TABLES `flarum_login_providers` WRITE;
/*!40000 ALTER TABLE `flarum_login_providers` DISABLE KEYS */;
/*!40000 ALTER TABLE `flarum_login_providers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flarum_migrations`
--

DROP TABLE IF EXISTS `flarum_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flarum_migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `extension` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=211 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flarum_migrations`
--

LOCK TABLES `flarum_migrations` WRITE;
/*!40000 ALTER TABLE `flarum_migrations` DISABLE KEYS */;
INSERT INTO `flarum_migrations` VALUES
(1,'2015_02_24_000000_create_access_tokens_table',NULL),
(2,'2015_02_24_000000_create_api_keys_table',NULL),
(3,'2015_02_24_000000_create_config_table',NULL),
(4,'2015_02_24_000000_create_discussions_table',NULL),
(5,'2015_02_24_000000_create_email_tokens_table',NULL),
(6,'2015_02_24_000000_create_groups_table',NULL),
(7,'2015_02_24_000000_create_notifications_table',NULL),
(8,'2015_02_24_000000_create_password_tokens_table',NULL),
(9,'2015_02_24_000000_create_permissions_table',NULL),
(10,'2015_02_24_000000_create_posts_table',NULL),
(11,'2015_02_24_000000_create_users_discussions_table',NULL),
(12,'2015_02_24_000000_create_users_groups_table',NULL),
(13,'2015_02_24_000000_create_users_table',NULL),
(14,'2015_09_15_000000_create_auth_tokens_table',NULL),
(15,'2015_09_20_224327_add_hide_to_discussions',NULL),
(16,'2015_09_22_030432_rename_notification_read_time',NULL),
(17,'2015_10_07_130531_rename_config_to_settings',NULL),
(18,'2015_10_24_194000_add_ip_address_to_posts',NULL),
(19,'2015_12_05_042721_change_access_tokens_columns',NULL),
(20,'2015_12_17_194247_change_settings_value_column_to_text',NULL),
(21,'2016_02_04_095452_add_slug_to_discussions',NULL),
(22,'2017_04_07_114138_add_is_private_to_discussions',NULL),
(23,'2017_04_07_114138_add_is_private_to_posts',NULL),
(24,'2018_01_11_093900_change_access_tokens_columns',NULL),
(25,'2018_01_11_094000_change_access_tokens_add_foreign_keys',NULL),
(26,'2018_01_11_095000_change_api_keys_columns',NULL),
(27,'2018_01_11_101800_rename_auth_tokens_to_registration_tokens',NULL),
(28,'2018_01_11_102000_change_registration_tokens_rename_id_to_token',NULL),
(29,'2018_01_11_102100_change_registration_tokens_created_at_to_datetime',NULL),
(30,'2018_01_11_120604_change_posts_table_to_innodb',NULL),
(31,'2018_01_11_155200_change_discussions_rename_columns',NULL),
(32,'2018_01_11_155300_change_discussions_add_foreign_keys',NULL),
(33,'2018_01_15_071700_rename_users_discussions_to_discussion_user',NULL),
(34,'2018_01_15_071800_change_discussion_user_rename_columns',NULL),
(35,'2018_01_15_071900_change_discussion_user_add_foreign_keys',NULL),
(36,'2018_01_15_072600_change_email_tokens_rename_id_to_token',NULL),
(37,'2018_01_15_072700_change_email_tokens_add_foreign_keys',NULL),
(38,'2018_01_15_072800_change_email_tokens_created_at_to_datetime',NULL),
(39,'2018_01_18_130400_rename_permissions_to_group_permission',NULL),
(40,'2018_01_18_130500_change_group_permission_add_foreign_keys',NULL),
(41,'2018_01_18_130600_rename_users_groups_to_group_user',NULL),
(42,'2018_01_18_130700_change_group_user_add_foreign_keys',NULL),
(43,'2018_01_18_133000_change_notifications_columns',NULL),
(44,'2018_01_18_133100_change_notifications_add_foreign_keys',NULL),
(45,'2018_01_18_134400_change_password_tokens_rename_id_to_token',NULL),
(46,'2018_01_18_134500_change_password_tokens_add_foreign_keys',NULL),
(47,'2018_01_18_134600_change_password_tokens_created_at_to_datetime',NULL),
(48,'2018_01_18_135000_change_posts_rename_columns',NULL),
(49,'2018_01_18_135100_change_posts_add_foreign_keys',NULL),
(50,'2018_01_30_112238_add_fulltext_index_to_discussions_title',NULL),
(51,'2018_01_30_220100_create_post_user_table',NULL),
(52,'2018_01_30_222900_change_users_rename_columns',NULL),
(55,'2018_09_15_041340_add_users_indicies',NULL),
(56,'2018_09_15_041828_add_discussions_indicies',NULL),
(57,'2018_09_15_043337_add_notifications_indices',NULL),
(58,'2018_09_15_043621_add_posts_indices',NULL),
(59,'2018_09_22_004100_change_registration_tokens_columns',NULL),
(60,'2018_09_22_004200_create_login_providers_table',NULL),
(61,'2018_10_08_144700_add_shim_prefix_to_group_icons',NULL),
(62,'2019_10_12_195349_change_posts_add_discussion_foreign_key',NULL),
(63,'2020_03_19_134512_change_discussions_default_comment_count',NULL),
(64,'2020_04_21_130500_change_permission_groups_add_is_hidden',NULL),
(65,'2021_03_02_040000_change_access_tokens_add_type',NULL),
(66,'2021_03_02_040500_change_access_tokens_add_id',NULL),
(67,'2021_03_02_041000_change_access_tokens_add_title_ip_agent',NULL),
(68,'2021_04_18_040500_change_migrations_add_id_primary_key',NULL),
(69,'2021_04_18_145100_change_posts_content_column_to_mediumtext',NULL),
(70,'2018_07_21_000000_seed_default_groups',NULL),
(71,'2018_07_21_000100_seed_default_group_permissions',NULL),
(72,'2021_05_10_000000_rename_permissions',NULL),
(73,'2015_09_21_011527_add_is_approved_to_discussions','flarum-approval'),
(74,'2015_09_21_011706_add_is_approved_to_posts','flarum-approval'),
(75,'2017_07_22_000000_add_default_permissions','flarum-approval'),
(76,'2018_09_29_060444_replace_emoji_shorcuts_with_unicode','flarum-emoji'),
(77,'2015_09_02_000000_add_flags_read_time_to_users_table','flarum-flags'),
(78,'2015_09_02_000000_create_flags_table','flarum-flags'),
(79,'2017_07_22_000000_add_default_permissions','flarum-flags'),
(80,'2018_06_27_101500_change_flags_rename_time_to_created_at','flarum-flags'),
(81,'2018_06_27_101600_change_flags_add_foreign_keys','flarum-flags'),
(82,'2018_06_27_105100_change_users_rename_flags_read_time_to_read_flags_at','flarum-flags'),
(83,'2018_09_15_043621_add_flags_indices','flarum-flags'),
(84,'2019_10_22_000000_change_reason_text_col_type','flarum-flags'),
(85,'2015_05_11_000000_create_posts_likes_table','flarum-likes'),
(86,'2015_09_04_000000_add_default_like_permissions','flarum-likes'),
(87,'2018_06_27_100600_rename_posts_likes_to_post_likes','flarum-likes'),
(88,'2018_06_27_100700_change_post_likes_add_foreign_keys','flarum-likes'),
(89,'2021_05_10_094200_add_created_at_to_post_likes_table','flarum-likes'),
(90,'2015_02_24_000000_add_locked_to_discussions','flarum-lock'),
(91,'2017_07_22_000000_add_default_permissions','flarum-lock'),
(92,'2018_09_15_043621_add_discussions_indices','flarum-lock'),
(93,'2021_03_25_000000_default_settings','flarum-markdown'),
(94,'2015_05_11_000000_create_mentions_posts_table','flarum-mentions'),
(95,'2015_05_11_000000_create_mentions_users_table','flarum-mentions'),
(96,'2018_06_27_102000_rename_mentions_posts_to_post_mentions_post','flarum-mentions'),
(97,'2018_06_27_102100_rename_mentions_users_to_post_mentions_user','flarum-mentions'),
(98,'2018_06_27_102200_change_post_mentions_post_rename_mentions_id_to_mentions_post_id','flarum-mentions'),
(99,'2018_06_27_102300_change_post_mentions_post_add_foreign_keys','flarum-mentions'),
(100,'2018_06_27_102400_change_post_mentions_user_rename_mentions_id_to_mentions_user_id','flarum-mentions'),
(101,'2018_06_27_102500_change_post_mentions_user_add_foreign_keys','flarum-mentions'),
(102,'2021_04_19_000000_set_default_settings','flarum-mentions'),
(103,'2015_02_24_000000_add_sticky_to_discussions','flarum-sticky'),
(104,'2017_07_22_000000_add_default_permissions','flarum-sticky'),
(105,'2018_09_15_043621_add_discussions_indices','flarum-sticky'),
(106,'2021_01_13_000000_add_discussion_last_posted_at_indices','flarum-sticky'),
(107,'2015_05_11_000000_add_subscription_to_users_discussions_table','flarum-subscriptions'),
(108,'2015_05_11_000000_add_suspended_until_to_users_table','flarum-suspend'),
(109,'2015_09_14_000000_rename_suspended_until_column','flarum-suspend'),
(110,'2017_07_22_000000_add_default_permissions','flarum-suspend'),
(111,'2018_06_27_111400_change_users_rename_suspend_until_to_suspended_until','flarum-suspend'),
(112,'2021_10_27_000000_add_suspend_reason_and_message','flarum-suspend'),
(113,'2015_02_24_000000_create_discussions_tags_table','flarum-tags'),
(114,'2015_02_24_000000_create_tags_table','flarum-tags'),
(115,'2015_02_24_000000_create_users_tags_table','flarum-tags'),
(116,'2015_02_24_000000_set_default_settings','flarum-tags'),
(117,'2015_10_19_061223_make_slug_unique','flarum-tags'),
(118,'2017_07_22_000000_add_default_permissions','flarum-tags'),
(119,'2018_06_27_085200_change_tags_columns','flarum-tags'),
(120,'2018_06_27_085300_change_tags_add_foreign_keys','flarum-tags'),
(121,'2018_06_27_090400_rename_users_tags_to_tag_user','flarum-tags'),
(122,'2018_06_27_100100_change_tag_user_rename_read_time_to_marked_as_read_at','flarum-tags'),
(123,'2018_06_27_100200_change_tag_user_add_foreign_keys','flarum-tags'),
(124,'2018_06_27_103000_rename_discussions_tags_to_discussion_tag','flarum-tags'),
(125,'2018_06_27_103100_add_discussion_tag_foreign_keys','flarum-tags'),
(126,'2019_04_21_000000_add_icon_to_tags_table','flarum-tags'),
(127,'2017_11_07_223624_discussions_add_views','michaelbelgium-discussion-views'),
(128,'2018_11_30_141817_discussions_rename_views','michaelbelgium-discussion-views'),
(129,'2020_01_11_220612_add_discussionviews_table','michaelbelgium-discussion-views'),
(130,'2020_01_17_110823_add_default_settings','michaelbelgium-discussion-views'),
(131,'2020_01_19_131055_add_viewlist_settings','michaelbelgium-discussion-views'),
(132,'2021_09_15_000000_add_extra_settings','michaelbelgium-discussion-views'),
(133,'2019_11_04_000001_add_columns_to_discussions_table','fof-best-answer'),
(134,'2019_11_04_000002_add_foreign_keys_to_best_anwer_columns','fof-best-answer'),
(135,'2019_11_04_000003_migrate_extension_settings','fof-best-answer'),
(136,'2020_02_04_205300_add_best_answer_set_timestamp','fof-best-answer'),
(137,'2021_08_09_add_qna_column_to_tags_table','fof-best-answer'),
(138,'2021_08_10_add_reminders_column_to_tags_table','fof-best-answer'),
(139,'2021_08_15_migrate_reminder_settings','fof-best-answer'),
(140,'2021_09_10_add_default_filter_option_setting','fof-best-answer'),
(141,'2022_05_19_000000_add_best_answer_count_to_users_table','fof-best-answer'),
(142,'2019_08_02_171300_create_drafts_table','fof-drafts'),
(143,'2019_08_02_172100_add_drafts_foreign_keys','fof-drafts'),
(144,'2019_08_03_171600_add_default_permissions','fof-drafts'),
(145,'2019_08_04_151000_change_content_to_medium_text','fof-drafts'),
(146,'2020_05_24_000000_add_scheduled_at_column','fof-drafts'),
(147,'2020_05_24_000001_add_schedule_post_permission','fof-drafts'),
(148,'2020_05_24_000002_add_error_and_ip_columns','fof-drafts'),
(149,'2020_05_24_000003_add_scheduled_post_setting','fof-drafts'),
(150,'2020_08_16_000000_add_extra_to_drafts','fof-drafts'),
(151,'2017_03_08_102708_add_emailed_to_posts','fof-filter'),
(152,'2019_07_05_022521_add_auto_mod_to_posts','fof-filter'),
(153,'2021_09_12_modify_badwords_delimiter','fof-filter'),
(154,'2019_06_11_000000_add_subscription_to_users_tags_table','fof-follow-tags'),
(155,'2019_06_28_000000_add_hide_subscription_option','fof-follow-tags'),
(156,'2021_01_22_000000_add_indicies','fof-follow-tags'),
(157,'2022_05_20_000000_add_timestamps_to_tag_user_table','fof-follow-tags'),
(158,'2019_07_09_000000_create_post_votes_table','fof-gamification'),
(159,'2019_07_09_000001_add_attributes_to_users','fof-gamification'),
(160,'2019_07_09_000002_add_votes_and_hotness_to_discussions','fof-gamification'),
(161,'2019_07_09_000003_add_default_permissions','fof-gamification'),
(162,'2019_07_09_000004_create_rank_users_table','fof-gamification'),
(163,'2019_07_09_000005_migrate_extension_settings','fof-gamification'),
(164,'2019_07_10_000000_create_ranks_table','fof-gamification'),
(165,'2019_08_09_000000_add_votes_foreign_keys','fof-gamification'),
(166,'2020_07_07_000000_add_prefix_to_all_settings','fof-gamification'),
(167,'2020_07_09_000000_change_post_votes_type_column','fof-gamification'),
(168,'2020_07_11_000000_change_permission_name','fof-gamification'),
(169,'2020_08_25_000000_update_tag_permission_names','fof-gamification'),
(170,'2021_10_27_000000_add_index_to_votes_column','fof-gamification'),
(171,'2021_10_28_000000_create_alternate_post_voting_ui_setting','fof-gamification'),
(172,'2022_02_04_000000_add_timestamps_to_post_votes_table','fof-gamification'),
(173,'2022_02_06_000000_add_default_notification_permissions','fof-gamification'),
(174,'2016_02_13_000000_create_links_table','fof-links'),
(175,'2016_04_19_065618_change_links_columns','fof-links'),
(176,'2020_03_16_000000_add_child_links','fof-links'),
(177,'2020_09_10_000000_add_icon_to_links_table','fof-links'),
(178,'2021_01_17_000000_add_registered_users_only_to_links_table','fof-links'),
(179,'2021_01_17_000001_add_visibility_to_links_table','fof-links'),
(180,'2021_01_17_000001_drop_registered_users_only_column_and_migrate_options','fof-links'),
(181,'2022_01_04_000000_create_redirections_table','fof-merge-discussions'),
(182,'2020_02_25_000001_create_moderator_notes_table','fof-moderator-notes'),
(183,'2020_02_25_000002_add_default_permissions','fof-moderator-notes'),
(184,'2020_02_29_000001_modify_users_notes_foreign_keys','fof-moderator-notes'),
(185,'2020_04_31_000000_format_note_content_for_renderer','fof-moderator-notes'),
(186,'2016_04_11_182821__create_pages_table','fof-pages'),
(187,'2016_08_28_180020_add_is_html','fof-pages'),
(188,'2020_02_26_010000_add_is_restricted','fof-pages'),
(189,'2020_09_09_173600_increase_char_limit','fof-pages'),
(190,'2020_02_06_01_rename_flagrow_permissions','fof-upload'),
(191,'2020_02_06_02_rename_flagrow_settings','fof-upload'),
(192,'2020_02_06_03_rename_flagrow_tables','fof-upload'),
(193,'2020_02_06_04_remove_flagrow_migrations','fof-upload'),
(194,'2020_02_06_05_create_files_table','fof-upload'),
(195,'2020_02_06_06_create_downloads_table','fof-upload'),
(196,'2020_02_06_07_alter_downloads_add_post_constraint','fof-upload'),
(197,'2021_02_11_01_add_uploads_view_and_delete_permissions','fof-upload'),
(198,'2021_03_13_000000_alter_upload_files_add_hidden_from_media_manager','fof-upload'),
(199,'2021_03_13_set_created_at_to_default_as_current_timestamp','fof-upload'),
(200,'2021_09_30_000000_add_index_actor_id_and_hide_media','fof-upload'),
(201,'2020_07_01_000000_create_bio_column','fof-user-bio'),
(202,'2021_01_17_000000_set_default_bio_length','fof-user-bio'),
(203,'2019_06_10_000000_rename_permissions','fof-user-directory'),
(204,'2020_03_13_000001_add_warnings_table','askvortsov-moderator-warnings'),
(205,'2020_03_13_000002_add_default_permissions','askvortsov-moderator-warnings'),
(206,'2020_05_31_000000_format_note_content_for_renderer','askvortsov-moderator-warnings'),
(207,'2020_11_23_000000_add_nickname_column','flarum-nicknames'),
(208,'2020_12_02_000001_set_default_permissions','flarum-nicknames'),
(209,'2021_11_16_000000_nickname_column_nullable','flarum-nicknames'),
(210,'2018_01_24_000000_add_migratetoflarum_old_password_to_users_table','migratetoflarum-old-passwords');
/*!40000 ALTER TABLE `flarum_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flarum_notifications`
--

DROP TABLE IF EXISTS `flarum_notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flarum_notifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `from_user_id` int(10) unsigned DEFAULT NULL,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject_id` int(10) unsigned DEFAULT NULL,
  `data` blob DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `read_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `flarum_notifications_from_user_id_foreign` (`from_user_id`),
  KEY `flarum_notifications_user_id_index` (`user_id`),
  CONSTRAINT `flarum_notifications_from_user_id_foreign` FOREIGN KEY (`from_user_id`) REFERENCES `flarum_users` (`id`) ON DELETE SET NULL,
  CONSTRAINT `flarum_notifications_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `flarum_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flarum_notifications`
--

LOCK TABLES `flarum_notifications` WRITE;
/*!40000 ALTER TABLE `flarum_notifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `flarum_notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flarum_pages`
--

DROP TABLE IF EXISTS `flarum_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flarum_pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` datetime NOT NULL,
  `edit_time` datetime DEFAULT NULL,
  `content` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT ' ',
  `is_hidden` tinyint(1) NOT NULL DEFAULT 0,
  `is_html` tinyint(1) NOT NULL DEFAULT 0,
  `is_restricted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flarum_pages`
--

LOCK TABLES `flarum_pages` WRITE;
/*!40000 ALTER TABLE `flarum_pages` DISABLE KEYS */;
INSERT INTO `flarum_pages` VALUES
(1,'Règles d\'utilisation du forum','regles','2022-07-28 19:57:37',NULL,'<r><p>Ce forum est mis à disposition des utilisateurs et des utilisatrices de la communauté francophone d\'Ubuntu par l\'association Ubuntu-fr. En vous inscrivant, en y participant, vous vous engagez à respecter la présente charte.</p>\n\n<H3><s>### </s>Loi</H3>\n\n<LIST type=\"decimal\"><LI><s>1.  </s>Le forum Ubuntu-fr, comme l\'association qui le met à disposition, est soumis à la législation française.</LI>\n<LI><s>2.  </s>Des données de connexion à caractère personnel (adresses IP…) sont récupérées de manière automatique et conservée à des fins de log, conformément à nos obligations légales. Nous nous engageons à ne les divulguer qu\'aux autorités compétentes, et uniquement sur injonction judiciaire.</LI>\n<LI><s>3.  </s>Les messages à caractère illicite (racisme, sexisme, homophobie, discrimination religieuse, contrefaçon, accès frauduleux…) sont bien évidemment interdits ; il en est de même pour les messages à caractère pornographique ou incitant à la violence.</LI></LIST>\n\n<H3><s>### </s>Objet</H3>\n\n<LIST type=\"decimal\"><LI><s>1.  </s>Le forum est composé de deux parties : les sections de support, dédiées au support autour d\'Ubuntu, et les sections diverses, ayant une vocation plus large. La priorité dans la gestion du forum est donnée aux sections de support, où la tolérance aux débordements est réduite.</LI>\n<LI><s>2.  </s>Ce forum étant dédié au support autour du système Ubuntu, les demandes d\'aide relatives à d\'autres systèmes n\'ont pas leur place en section support, exception faite des versions dérivées d\'Ubuntu n\'ayant pas de forum francophone dédié (pour une liste non-exhaustive des forums disponibles pour d\'autres systèmes, n\'hésitez pas à vous référer à <URL url=\"https://doc.ubuntu-fr.org/derivees_d_ubuntu\"><s>[</s>cette page<e>](https://doc.ubuntu-fr.org/derivees_d_ubuntu)</e></URL>. Des discussions à ce sujet peuvent cependant être postées dans les sections diverses.</LI>\n<LI><s>3.  </s>Le forum s\'adressant à des personnes de tous niveaux, aucune question ne saurait être considérée comme sujette à moquerie.</LI></LIST>\n\n<H3><s>### </s>Respect</H3>\n\n<LIST type=\"decimal\"><LI><s>1.  </s>Les membres du forum Ubuntu-fr doivent se comporter entre eux de manière respectueuse. Les insultes et dénigrements, envers les membres comme envers leurs choix d\'utilisation sont interdits, de même qu\'en support, les renvois sans ménagement à la lecture de la documentation (« RTFM ») ou les tournures visant à se moquer d\'un supposé manque d\'investissement de la personne demandeuse (ex : liens lmgtfy).</LI>\n<LI><s>2.  </s>Pour les demandes techniques, la priorité doit être donnée à une solution tenant compte de l\'environnement utilisateur. Les suggestions de migrations (changement d\'environnement de bureau, changement de système) ne doivent être abordées qu\'à titre complémentaire, ou si aucune autre solution n\'a pu être trouvée.</LI>\n<LI><s>3.  </s>Les contributions au forum sont bénévoles, sans aucune obligation de résultat : l\'aide apportée sur ce forum n\'est pas un dû et l\'urgence de la situation du point de vue de la personne demandeuse ne lui apporte aucune priorité. Les tags tels que « [Urgent] » sont interdits, hors cas d\'avertissement sérieux touchant l\'ensemble de la communauté (faille de sécurité critique…)</LI></LIST>\n\n<H3><s>### </s>Autonomie</H3>\n\n<LIST type=\"decimal\"><LI><s>1.  </s>Il est conseillé, avant de poser une question, de vérifier qu\'une réponse ne se trouve pas déjà dans la documentation ou dans un autre sujet du forum. Les doublons volontaires sont interdits. En revanche, les questions différentes doivent être traitées dans un sujet différent, afin de gagner en lisibilité. Les titres des discussions doivent être explicites.</LI>\n<LI><s>2.  </s>Autant que possible, les sujets d\'aide doivent rester auto-suffisants : dans le cas où la citation d\'une documentation externe serait requise pour résoudre un problème, il est demandé de mentionner explicitement les passages utiles de cette documentation dans son message, afin que celui-ci reste compréhensible même si la page externe contenant les informations venait à disparaître ou à être modifiée. De même, dans les sections diverses, il est recommandé de préciser en quelques courtes phrases l\'objet des liens postés.</LI>\n<LI><s>3.  </s>La possibilité de supprimer ses propres messages est laissée aux titulaires du statut de « membre » (hormis le premier message de la discussion, pour des raisons techniques), mais son usage doit être limité aux cas où cela n\'impacte pas la suite des discussions.</LI>\n<LI><s>4.  </s>Afin de faciliter le suivi des discussions, il est recommandé de mettre à jour leurs titres (ce qui se fait en éditant le premier message) afin de rajouter des tags tels que « [Résolu] ».</LI></LIST>\n\n<H3><s>### </s>Pollution</H3>\n\n<LIST type=\"decimal\"><LI><s>1.  </s>Le flood (succession de messages d\'un même auteur en un laps de temps court, y compris les « up »), le spam (messages indésirables visant par exemple à amener l\'internaute à cliquer sur un lien), le prosélytisme et les hors-sujets intentionnels sont interdits, tant sur le forum que par le biais de la messagerie privée.</LI>\n<LI><s>2.  </s>Les URL raccourcies ne permettant pas de savoir vers quel site on est redirigé, et aucune nécessité technique propre au forum ne nécessitant leur emploi, l\'usage des raccourcisseurs d\'URL est interdit.</LI>\n<LI><s>3.  </s>Les liens commerciaux et petites annonces commerciales sont soumis à autorisation préalable. La promotion de sites non-commerciaux sans rapport avec Ubuntu, ou le logiciel libre en général, est à éviter ; les signatures des membres pouvant toutefois êtres utilisées dans ce but.</LI>\n<LI><s>4.  </s>Les contraintes de connexion et de taille d\'écran de certains participants font que les images trop grandes (au delà de 300×300 pixels) ou trop lourdes (plus de 80 Ko) doivent être évitées. Utilisez si possible des miniatures, et privilégiez les hébergements respectueux de l\'utilisateur. N\'hésitez pas à vous référer à <URL url=\"https://doc.ubuntu-fr.org/capture_d_ecran#heberger_une_capture_d_ecran\"><s>[</s>cette page<e>](https://doc.ubuntu-fr.org/capture_d_ecran#heberger_une_capture_d_ecran)</e></URL> de la documentation.</LI>\n<LI><s>5.  </s>Ce forum n\'a aucune vocation commerciale, aussi tout message proposant des produits, services ou échanges financiers est interdit, sauf autorisation expresse de l\'équipe de modération.</LI></LIST>\n\n<H3><s>### </s>Politesse</H3>\n\n<LIST type=\"decimal\"><LI><s>1.  </s>La langue du forum est le français. Il est recommandé d\'être attentif à la lisibilité de ses messages, par la relecture et une attention portée à l\'orthographe, à la ponctuation, à la grammaire et à l\'aération du texte. Les fautes volontaires et l\'usage de langages « SMS » ou « l33t » sont indésirables. En cas d\'erreur, la possibilité est donnée d\'éditer son message.</LI>\n<LI><s>2.  </s>Cela ne doit néanmoins pas conduire à des attaques, des moqueries ou des détournements de sujet sur la forme d\'un message.</LI>\n<LI><s>3.  </s>Le moteur BBCode fourni par le forum permet une mise en forme efficace : n\'hésitez pas à vous en servir. En particulier, les citations doivent utiliser des balises <C><s>`</s>[quote]<e>`</e></C>, avec précision de l\'auteur des propos ; et les retours de commandes doivent être placées entre balises <C><s>`</s>[code]<e>`</e></C>. Les abus de formatage (citations plus longues que le message y répondant ; usage important ou systématique des couleurs ou smileys…) sont cependant à éviter.</LI>\n<LI><s>4.  </s>Dire bonjour et remercier les contributeurs n\'est jamais superflu. D\'une manière générale, le respect du <URL url=\"http://wiki.ubuntu-fr.org/codedeconduite\"><s>[</s>code de conduite<e>](http://wiki.ubuntu-fr.org/codedeconduite)</e></URL> et de la <URL url=\"http://www.atoute.org/n/article1.html\"><s>[</s>netiquette<e>](http://www.atoute.org/n/article1.html)</e></URL> sont préférables.</LI></LIST>\n\n<H3><s>### </s>Statuts</H3>\n\n<p>Hors cas spécifiques (dus par exemple à des problèmes de comportement), les personnes disposant d\'un compte sur le forum sont réparties dans les groupes suivants :</p>  \n\n<LIST><LI><s>*   </s><STRONG><s>**</s>Nouveaux<e>**</e></STRONG> : contenant les personnes récemment inscrites sur le forum, ce groupe dispose d\'accès limités à certaines sections ou fonctionnalités potentiellement sensibles ou problématiques ; mais peut participer sans restriction aux sections de support et au café.</LI>\n<LI><s>*   </s><STRONG><s>**</s>Membres<e>**</e></STRONG> : la majorité des membres « ordinaires » du forum, disposant des droits normaux. Le passage du groupe « Nouveaux » au groupe « Membres » se fait de manière semi-automatique, selon l\'implication, aucune demande préalable n\'étant requise.</LI>\n<LI><s>*   </s><STRONG><s>**</s>Traders<e>**</e></STRONG> : les membres de ce groupe ont obtenu une autorisation spéciale concernant des activités commerciales (notamment, le droit de poster dans la section « Revendeurs libres »). L\'accès au statut de Trader se fait sur demande motivée auprès de l\'équipe de modération, qui décidera ou non d\'y donner suite et motivera sa décision.</LI>\n<LI><s>*   </s><STRONG><s>**</s>Modérateurs/Administrateurs<e>**</e></STRONG> : l\'équipe technique et de modération du forum. Les volontaires souhaitant intégrer l\'équipe de modération doivent envoyer une demande motivée à l\'équipe de modération, qui décidera ou non d\'y donner suite et motivera sa décision.</LI></LIST>\n\n<H3><s>### </s>Modération</H3>\n\n<LIST type=\"decimal\"><LI><s>1.  </s>L\'équipe de modération en charge du forum a toute légitimité à estimer les manquements aux règles ci-dessus. Elle ne peut cependant pas suivre l\'activité de l\'ensemble du forum : les messages problématiques doivent lui être signalés par le biais de la fonction prévue à cet effet (laquelle ne doit pas être utilisée pour demander de l\'aide technique). Les décisions étant collégiales et dépendant de la présence d\'une équipe elle aussi bénévole, aucun délai de traitement n\'est garanti.</LI>\n<LI><s>2.  </s>Les actions de modération peuvent consister en une modification — manuelle ou automatique — des parties problématiques d\'un message, à un retrait du message entier, voire de l\'avatar ou de la signature d\'un membre. Les discussions peuvent être scindées, fusionnées, déplacées ou fermées selon les besoins. Les membres transgressant les règles peuvent être exclus du forum de façon temporaire ou définitive (toute réinscription visant à contourner une telle exclusion justifierait une exclusion du nouveau compte).</LI>\n<LI><s>3.  </s>Les décisions de modération sont souveraines et ne doivent pas être remises en cause sur le forum. Les membres peuvent toutefois faire part de leurs questions ou remarques sur la liste de diffusion prévue à cet effet (moderateurs [at] ubuntu-fr.org).</LI></LIST></r>',0,0,0);
/*!40000 ALTER TABLE `flarum_pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flarum_password_tokens`
--

DROP TABLE IF EXISTS `flarum_password_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flarum_password_tokens` (
  `token` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`token`),
  KEY `flarum_password_tokens_user_id_foreign` (`user_id`),
  CONSTRAINT `flarum_password_tokens_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `flarum_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flarum_password_tokens`
--

LOCK TABLES `flarum_password_tokens` WRITE;
/*!40000 ALTER TABLE `flarum_password_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `flarum_password_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flarum_post_likes`
--

DROP TABLE IF EXISTS `flarum_post_likes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flarum_post_likes` (
  `post_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`post_id`,`user_id`),
  KEY `flarum_post_likes_user_id_foreign` (`user_id`),
  CONSTRAINT `flarum_post_likes_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `flarum_posts` (`id`) ON DELETE CASCADE,
  CONSTRAINT `flarum_post_likes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `flarum_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flarum_post_likes`
--

LOCK TABLES `flarum_post_likes` WRITE;
/*!40000 ALTER TABLE `flarum_post_likes` DISABLE KEYS */;
/*!40000 ALTER TABLE `flarum_post_likes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flarum_post_mentions_post`
--

DROP TABLE IF EXISTS `flarum_post_mentions_post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flarum_post_mentions_post` (
  `post_id` int(10) unsigned NOT NULL,
  `mentions_post_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`post_id`,`mentions_post_id`),
  KEY `flarum_post_mentions_post_mentions_post_id_foreign` (`mentions_post_id`),
  CONSTRAINT `flarum_post_mentions_post_mentions_post_id_foreign` FOREIGN KEY (`mentions_post_id`) REFERENCES `flarum_posts` (`id`) ON DELETE CASCADE,
  CONSTRAINT `flarum_post_mentions_post_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `flarum_posts` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flarum_post_mentions_post`
--

LOCK TABLES `flarum_post_mentions_post` WRITE;
/*!40000 ALTER TABLE `flarum_post_mentions_post` DISABLE KEYS */;
/*!40000 ALTER TABLE `flarum_post_mentions_post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flarum_post_mentions_user`
--

DROP TABLE IF EXISTS `flarum_post_mentions_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flarum_post_mentions_user` (
  `post_id` int(10) unsigned NOT NULL,
  `mentions_user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`post_id`,`mentions_user_id`),
  KEY `flarum_post_mentions_user_mentions_user_id_foreign` (`mentions_user_id`),
  CONSTRAINT `flarum_post_mentions_user_mentions_user_id_foreign` FOREIGN KEY (`mentions_user_id`) REFERENCES `flarum_users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `flarum_post_mentions_user_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `flarum_posts` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flarum_post_mentions_user`
--

LOCK TABLES `flarum_post_mentions_user` WRITE;
/*!40000 ALTER TABLE `flarum_post_mentions_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `flarum_post_mentions_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flarum_post_user`
--

DROP TABLE IF EXISTS `flarum_post_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flarum_post_user` (
  `post_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`post_id`,`user_id`),
  KEY `flarum_post_user_user_id_foreign` (`user_id`),
  CONSTRAINT `flarum_post_user_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `flarum_posts` (`id`) ON DELETE CASCADE,
  CONSTRAINT `flarum_post_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `flarum_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flarum_post_user`
--

LOCK TABLES `flarum_post_user` WRITE;
/*!40000 ALTER TABLE `flarum_post_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `flarum_post_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flarum_post_votes`
--

DROP TABLE IF EXISTS `flarum_post_votes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flarum_post_votes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `value` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `flarum_post_votes_post_id_foreign` (`post_id`),
  CONSTRAINT `flarum_post_votes_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `flarum_posts` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flarum_post_votes`
--

LOCK TABLES `flarum_post_votes` WRITE;
/*!40000 ALTER TABLE `flarum_post_votes` DISABLE KEYS */;
/*!40000 ALTER TABLE `flarum_post_votes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flarum_posts`
--

DROP TABLE IF EXISTS `flarum_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flarum_posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `discussion_id` int(10) unsigned NOT NULL,
  `number` int(10) unsigned DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT ' ',
  `edited_at` datetime DEFAULT NULL,
  `edited_user_id` int(10) unsigned DEFAULT NULL,
  `hidden_at` datetime DEFAULT NULL,
  `hidden_user_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_private` tinyint(1) NOT NULL DEFAULT 0,
  `is_approved` tinyint(1) NOT NULL DEFAULT 1,
  `emailed` tinyint(1) NOT NULL DEFAULT 0,
  `auto_mod` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `flarum_posts_discussion_id_number_unique` (`discussion_id`,`number`),
  KEY `flarum_posts_edited_user_id_foreign` (`edited_user_id`),
  KEY `flarum_posts_hidden_user_id_foreign` (`hidden_user_id`),
  KEY `flarum_posts_discussion_id_number_index` (`discussion_id`,`number`),
  KEY `flarum_posts_discussion_id_created_at_index` (`discussion_id`,`created_at`),
  KEY `flarum_posts_user_id_created_at_index` (`user_id`,`created_at`),
  FULLTEXT KEY `content` (`content`),
  CONSTRAINT `flarum_posts_discussion_id_foreign` FOREIGN KEY (`discussion_id`) REFERENCES `flarum_discussions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `flarum_posts_edited_user_id_foreign` FOREIGN KEY (`edited_user_id`) REFERENCES `flarum_users` (`id`) ON DELETE SET NULL,
  CONSTRAINT `flarum_posts_hidden_user_id_foreign` FOREIGN KEY (`hidden_user_id`) REFERENCES `flarum_users` (`id`) ON DELETE SET NULL,
  CONSTRAINT `flarum_posts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `flarum_users` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flarum_posts`
--

LOCK TABLES `flarum_posts` WRITE;
/*!40000 ALTER TABLE `flarum_posts` DISABLE KEYS */;
/*!40000 ALTER TABLE `flarum_posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flarum_rank_users`
--

DROP TABLE IF EXISTS `flarum_rank_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flarum_rank_users` (
  `rank_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`rank_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flarum_rank_users`
--

LOCK TABLES `flarum_rank_users` WRITE;
/*!40000 ALTER TABLE `flarum_rank_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `flarum_rank_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flarum_ranks`
--

DROP TABLE IF EXISTS `flarum_ranks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flarum_ranks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `points` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flarum_ranks`
--

LOCK TABLES `flarum_ranks` WRITE;
/*!40000 ALTER TABLE `flarum_ranks` DISABLE KEYS */;
/*!40000 ALTER TABLE `flarum_ranks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flarum_registration_tokens`
--

DROP TABLE IF EXISTS `flarum_registration_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flarum_registration_tokens` (
  `token` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `identifier` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_attributes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flarum_registration_tokens`
--

LOCK TABLES `flarum_registration_tokens` WRITE;
/*!40000 ALTER TABLE `flarum_registration_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `flarum_registration_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flarum_settings`
--

DROP TABLE IF EXISTS `flarum_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flarum_settings` (
  `key` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flarum_settings`
--

LOCK TABLES `flarum_settings` WRITE;
/*!40000 ALTER TABLE `flarum_settings` DISABLE KEYS */;
INSERT INTO `flarum_settings` VALUES
('allow_post_editing','reply'),
('allow_renaming','10'),
('allow_sign_up','1'),
('custom_header','<link rel=\"stylesheet\" href=\"//ufr-cms.localhost/css/main.css\">\n<link rel=\"stylesheet\" href=\"/assets/css/forum.css\">\n<link rel=\"stylesheet\" href=\"//ufr-cms.localhost/css/print.css\" media=\"print\">\n<script type=\"module\" src=\"//ufr-cms.localhost/js/app.js\"></script>\n<script type=\"module\" src=\"/assets/js/forum.js\"></script>\n\n<header>\n  <div class=\"topbar\">\n    <a href=\"/\" id=\"logo\">\n      <img src=\"//ufr-cms.localhost/img/logo.svg\" alt=\"ubuntu-fr\">\n    </a>\n    <div id=\"sandwich\">\n      <div></div>\n      <div></div>\n      <div></div>\n    </div>\n  </div>\n  <div class=\"navbar\">\n    <nav>\n      <a href=\"//ufr-cms.localhost\">Accueil</a>\n      <a href=\"//ufr-doc.localhost\">Documentation</a>\n      <a href=\"/\" class=\"active\">Forum</a>\n      <a href=\"//ufr-cms.localhost/about\">À propos</a>\n    </nav>\n    <div class=\"switchers\">\n      <div class=\"switcher\" id=\"switcher-wide\"><div></div><div></div></div>\n      <div href=\"#\" class=\"switcher\" id=\"switcher-light\"></div>\n      <div href=\"#\" class=\"switcher\" id=\"switcher-dark\"></div>\n    </div>\n  </div>\n</header>\n'),
('custom_less',''),
('default_locale','fr'),
('default_route','/all'),
('display_name_driver','nickname'),
('extensions_enabled','[\"flarum-flags\",\"flarum-subscriptions\",\"flarum-tags\",\"flarum-suspend\",\"flarum-approval\",\"fof-follow-tags\",\"migratetoflarum-old-passwords\",\"migratetoflarum-fake-data\",\"michaelbelgium-discussion-views\",\"fof-user-directory\",\"fof-user-bio\",\"fof-upload\",\"fof-split\",\"fof-spamblock\",\"fof-prevent-necrobumping\",\"fof-pages\",\"fof-moderator-notes\",\"fof-merge-discussions\",\"fof-links\",\"fof-gamification\",\"fof-formatting\",\"fof-filter\",\"fof-drafts\",\"fof-best-answer\",\"flarum-sticky\",\"flarum-statistics\",\"flarum-nicknames\",\"flarum-mentions\",\"flarum-markdown\",\"flarum-lock\",\"flarum-lang-french\",\"flarum-lang-english\",\"flarum-emoji\",\"flarum-bbcode\",\"askvortsov-moderator-warnings\"]'),
('favicon_path','favicon-5b95rzkk.png'),
('flarum-markdown.mdarea','1'),
('flarum-mentions.allow_username_format','1'),
('flarum-nicknames.max','150'),
('flarum-nicknames.min','1'),
('flarum-nicknames.random_username',''),
('flarum-nicknames.regex',''),
('flarum-nicknames.set_on_registration',''),
('flarum-nicknames.unique',''),
('flarum-tags.max_primary_tags','1'),
('flarum-tags.max_secondary_tags','3'),
('flarum-tags.min_primary_tags','1'),
('flarum-tags.min_secondary_tags','0'),
('fof-best-answer.allow_select_own_post','1'),
('fof-best-answer.schedule_on_one_server',''),
('fof-best-answer.select_best_answer_reminder_days',''),
('fof-best-answer.show_filter_dropdown','1'),
('fof-best-answer.stop_overnight',''),
('fof-best-answer.store_log_output',''),
('fof-best-answer.use_alternative_ui',''),
('fof-drafts.enable_scheduled_drafts','1'),
('fof-formatting.plugin.autoimage','1'),
('fof-formatting.plugin.autovideo','1'),
('fof-formatting.plugin.fancypants',''),
('fof-formatting.plugin.htmlentities',''),
('fof-formatting.plugin.mediaembed',''),
('fof-formatting.plugin.pipetables',''),
('fof-formatting.plugin.tasklists',''),
('fof-gamification.altPostVotingUi','0'),
('fof-merge-discussions.search_limit',''),
('fof-prevent-necrobumping.days','30'),
('fof-prevent-necrobumping.days.tags.1',''),
('fof-prevent-necrobumping.days.tags.10',''),
('fof-prevent-necrobumping.days.tags.11',''),
('fof-prevent-necrobumping.days.tags.12',''),
('fof-prevent-necrobumping.days.tags.13',''),
('fof-prevent-necrobumping.days.tags.14',''),
('fof-prevent-necrobumping.days.tags.15',''),
('fof-prevent-necrobumping.days.tags.16',''),
('fof-prevent-necrobumping.days.tags.17',''),
('fof-prevent-necrobumping.days.tags.18',''),
('fof-prevent-necrobumping.days.tags.19',''),
('fof-prevent-necrobumping.days.tags.2',''),
('fof-prevent-necrobumping.days.tags.20',''),
('fof-prevent-necrobumping.days.tags.21',''),
('fof-prevent-necrobumping.days.tags.22',''),
('fof-prevent-necrobumping.days.tags.23',''),
('fof-prevent-necrobumping.days.tags.24',''),
('fof-prevent-necrobumping.days.tags.25',''),
('fof-prevent-necrobumping.days.tags.26',''),
('fof-prevent-necrobumping.days.tags.27',''),
('fof-prevent-necrobumping.days.tags.28',''),
('fof-prevent-necrobumping.days.tags.29',''),
('fof-prevent-necrobumping.days.tags.3',''),
('fof-prevent-necrobumping.days.tags.30',''),
('fof-prevent-necrobumping.days.tags.31',''),
('fof-prevent-necrobumping.days.tags.32',''),
('fof-prevent-necrobumping.days.tags.33',''),
('fof-prevent-necrobumping.days.tags.34',''),
('fof-prevent-necrobumping.days.tags.35',''),
('fof-prevent-necrobumping.days.tags.36',''),
('fof-prevent-necrobumping.days.tags.37',''),
('fof-prevent-necrobumping.days.tags.38',''),
('fof-prevent-necrobumping.days.tags.39',''),
('fof-prevent-necrobumping.days.tags.4',''),
('fof-prevent-necrobumping.days.tags.40',''),
('fof-prevent-necrobumping.days.tags.5',''),
('fof-prevent-necrobumping.days.tags.6',''),
('fof-prevent-necrobumping.days.tags.7',''),
('fof-prevent-necrobumping.days.tags.8',''),
('fof-prevent-necrobumping.days.tags.9',''),
('fof-prevent-necrobumping.message.agreement','Souhaitez-vous tout de même y participer ?'),
('fof-prevent-necrobumping.message.description','Il est probable qu\'elle soit terminée et qu\'il vaille mieux créer un nouveau sujet.'),
('fof-prevent-necrobumping.message.title','Cette conversation a plus d\'un mois.'),
('fof-user-bio.maxLength','10000'),
('fof-user-directory-link',''),
('fof-user-directory.default-sort',''),
('fof-user-directory.disable-global-search-source',''),
('fof-user-directory.use-small-cards',''),
('forum_description','Communauté francophone des utilisateurs d\'Ubuntu'),
('forum_title','Forum Ubuntu-fr'),
('logo_path',NULL),
('mail_driver','smtp'),
('mail_encryption',''),
('mail_from','noreply@ufr-forum.localhost'),
('mail_host','mailhog'),
('mail_password',''),
('mail_port','1025'),
('mail_username',''),
('michaelbelgium-discussionviews.abbr_numbers','0'),
('michaelbelgium-discussionviews.max_listcount','5'),
('michaelbelgium-discussionviews.show_filter','0'),
('michaelbelgium-discussionviews.show_footer_viewlist','0'),
('michaelbelgium-discussionviews.show_viewlist','0'),
('michaelbelgium-discussionviews.track_guests','1'),
('michaelbelgium-discussionviews.track_unique','1'),
('show_language_selector',''),
('slug_driver_Flarum\\User\\User','default'),
('theme_colored_header','0'),
('theme_dark_mode','0'),
('theme_primary_color','#E95420'),
('theme_secondary_color','#4b4443'),
('version','1.3.0'),
('welcome_message','Ceci est un forum de test, le forum officiel est à l\'adresse <a href=\"https://forum.ubuntu-fr.org\" target=\"_blank\">forum.ubuntu-fr.org</a>.'),
('welcome_title','Bienvenue sur le forum Ubuntu-fr');
/*!40000 ALTER TABLE `flarum_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flarum_tag_user`
--

DROP TABLE IF EXISTS `flarum_tag_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flarum_tag_user` (
  `user_id` int(10) unsigned NOT NULL,
  `tag_id` int(10) unsigned NOT NULL,
  `marked_as_read_at` datetime DEFAULT NULL,
  `is_hidden` tinyint(1) NOT NULL DEFAULT 0,
  `subscription` enum('follow','lurk','ignore','hide') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`user_id`,`tag_id`),
  KEY `flarum_tag_user_tag_id_foreign` (`tag_id`),
  KEY `flarum_tag_user_user_id_subscription_index` (`user_id`,`subscription`),
  KEY `flarum_tag_user_subscription_index` (`subscription`),
  CONSTRAINT `flarum_tag_user_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `flarum_tags` (`id`) ON DELETE CASCADE,
  CONSTRAINT `flarum_tag_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `flarum_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flarum_tag_user`
--

LOCK TABLES `flarum_tag_user` WRITE;
/*!40000 ALTER TABLE `flarum_tag_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `flarum_tag_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flarum_tags`
--

DROP TABLE IF EXISTS `flarum_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flarum_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `background_path` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `background_mode` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `default_sort` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_restricted` tinyint(1) NOT NULL DEFAULT 0,
  `is_hidden` tinyint(1) NOT NULL DEFAULT 0,
  `discussion_count` int(10) unsigned NOT NULL DEFAULT 0,
  `last_posted_at` datetime DEFAULT NULL,
  `last_posted_discussion_id` int(10) unsigned DEFAULT NULL,
  `last_posted_user_id` int(10) unsigned DEFAULT NULL,
  `icon` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_qna` tinyint(1) NOT NULL DEFAULT 0,
  `qna_reminders` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `flarum_tags_slug_unique` (`slug`),
  KEY `flarum_tags_parent_id_foreign` (`parent_id`),
  KEY `flarum_tags_last_posted_user_id_foreign` (`last_posted_user_id`),
  KEY `flarum_tags_last_posted_discussion_id_foreign` (`last_posted_discussion_id`),
  CONSTRAINT `flarum_tags_last_posted_discussion_id_foreign` FOREIGN KEY (`last_posted_discussion_id`) REFERENCES `flarum_discussions` (`id`) ON DELETE SET NULL,
  CONSTRAINT `flarum_tags_last_posted_user_id_foreign` FOREIGN KEY (`last_posted_user_id`) REFERENCES `flarum_users` (`id`) ON DELETE SET NULL,
  CONSTRAINT `flarum_tags_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `flarum_tags` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flarum_tags`
--

LOCK TABLES `flarum_tags` WRITE;
/*!40000 ALTER TABLE `flarum_tags` DISABLE KEYS */;
INSERT INTO `flarum_tags` VALUES
(1,'Débuter','debuter','Questions et informations avant l\'installation','#E95420',NULL,NULL,0,NULL,NULL,0,0,0,NULL,NULL,NULL,'',1,0),
(2,'résolu','resolu','Sujets résolus','#41a90a',NULL,NULL,NULL,NULL,NULL,0,1,0,NULL,NULL,NULL,'fas fa-check',0,0),
(3,'Matériel','materiel','Questions portant sur les ordinateurs ou les périphériques','#E95420',NULL,NULL,1,NULL,NULL,0,0,0,NULL,NULL,NULL,'',1,0),
(4,'Logiciel','logiciel','Support au sujet d\'Ubuntu ou de ses applications','#E95420',NULL,NULL,2,NULL,NULL,0,0,0,NULL,NULL,NULL,'',1,0),
(5,'Avancé','avance','Concernant les utilisations spécialisées','#E95420',NULL,NULL,4,NULL,NULL,0,0,0,NULL,NULL,NULL,'',1,0),
(6,'Installation','installation','Concernant l\'installation d\'Ubuntu','#333',NULL,NULL,0,1,NULL,0,0,0,NULL,NULL,NULL,'',1,0),
(7,'Affichage','affichage','Problèmes d\'affichage, questions sur les cartes graphiques','#333',NULL,NULL,0,3,NULL,0,0,0,NULL,NULL,NULL,'',1,0),
(8,'Réseau','reseau','Accès Internet, réseau, wifi','#333',NULL,NULL,1,3,NULL,0,0,0,NULL,NULL,NULL,'',1,0),
(9,'Imprimantes','imprimantes','','#333',NULL,NULL,2,3,NULL,0,0,0,NULL,NULL,NULL,'',1,0),
(10,'Scanners','scanners','','#333',NULL,NULL,3,3,NULL,0,0,0,NULL,NULL,NULL,'',1,0),
(11,'Gestion des logiciels','gestion','Installation ou mise à jour de logiciels, problèmes avec la logithèque, snap ou APT','#333',NULL,NULL,0,4,NULL,0,0,0,NULL,NULL,NULL,'',1,0),
(12,'Bureautique','bureautique','Édition de texte, comptabilité, présentations, etc.','#333',NULL,NULL,1,4,NULL,0,0,0,NULL,NULL,NULL,'',1,0),
(13,'Messagerie','messagerie','Clients mails, chat, visioconférences','#333',NULL,NULL,3,4,NULL,0,0,0,NULL,NULL,NULL,'',1,0),
(14,'Multimédia','multimedia','Lecture et configuration audio, vidéo et images','#333',NULL,NULL,4,4,NULL,0,0,0,NULL,NULL,NULL,'',1,0),
(15,'Graphisme','graphisme','Création graphique, photographique, design, manipulation d\'images','#333',NULL,NULL,5,4,NULL,0,0,0,NULL,NULL,NULL,'',1,0),
(16,'MAO','mao','Création musicale, édition audio','#333',NULL,NULL,7,4,NULL,0,0,0,NULL,NULL,NULL,'',1,0),
(17,'Création vidéo','video','Montage, animation, compositing, VJing','#333',NULL,NULL,6,4,NULL,0,0,0,NULL,NULL,NULL,'',1,0),
(18,'Jeux','jeux','','#333',NULL,NULL,9,4,NULL,0,0,0,NULL,NULL,NULL,'',1,0),
(19,'Terminal','terminal','Shell, ligne de commande, scripts','#333',NULL,NULL,10,4,NULL,0,0,0,NULL,NULL,NULL,'',1,0),
(20,'Accessibilité','accessibilite','Problématiques liées à l\'accessibilité. Elle concerne les façons de remédier aux difficultés d\'interaction avec le système dans son ensemble, en particulier pour les personnes en situation de handicap.','#333',NULL,NULL,12,4,NULL,0,0,0,NULL,NULL,NULL,'',1,0),
(21,'Navigateur web','navigateur','Firefox, Chrome, Vivaldi, etc.','#333',NULL,NULL,2,4,NULL,0,0,0,NULL,NULL,NULL,'',1,0),
(22,'Entreprise','entreprise','Pour les professionnels: ERPs, CRMs, logiciels de facturation, témoignages de déploiement…','#333',NULL,NULL,0,5,NULL,0,0,0,NULL,NULL,NULL,'',1,0),
(23,'Éducation','education','Au sujet de tous ces logiciels installés pour nos petits geeks','#333',NULL,NULL,8,4,NULL,0,0,0,NULL,NULL,NULL,'',1,0),
(24,'Serveur','serveur','Services web, CMS, Docker et configuration d\'Ubuntu en serveur','#333',NULL,NULL,1,5,NULL,0,0,0,NULL,NULL,NULL,'',1,0),
(25,'Environnement','environnement','Relatif aux environnements de bureau et à la personnalisation d\'Ubuntu','#E95420',NULL,NULL,3,NULL,NULL,0,0,0,NULL,NULL,NULL,'',1,0),
(26,'GNOME','gnome','','#333',NULL,NULL,0,25,NULL,0,0,0,NULL,NULL,NULL,'',1,0),
(27,'KDE','kde','','#333',NULL,NULL,1,25,NULL,0,0,0,NULL,NULL,NULL,'',1,0),
(28,'XFCE','xfce','','#333',NULL,NULL,4,25,NULL,0,0,0,NULL,NULL,NULL,'',1,0),
(29,'Mate','mate','','#333',NULL,NULL,3,25,NULL,0,0,0,NULL,NULL,NULL,'',1,0),
(30,'Budgie','budgie','','#333',NULL,NULL,2,25,NULL,0,0,0,NULL,NULL,NULL,'',1,0),
(31,'LXDE','lxde','','#333',NULL,NULL,5,25,NULL,0,0,0,NULL,NULL,NULL,'',1,0),
(32,'Virtualisation','virtualisation','Plusieurs Ubuntu sur un seul serveur ? C\'est possible ! Ici on parle de KVM, Virtualbox, XEN, vmware, virtuozzo…','#333',NULL,NULL,2,5,NULL,0,0,0,NULL,NULL,NULL,'',1,0),
(33,'Développement','developpement','Vous développez un logiciel pour Ubuntu - mais rencontrez des difficultés ? Venez demander aux aficionados du Python et du C++ ;)','#333',NULL,NULL,3,5,NULL,0,0,0,NULL,NULL,NULL,'',1,0),
(34,'Sécurité','securite','Logiciels et procédures pour sécuriser vos données, votre ordinateur et votre connexion.','#333',NULL,NULL,4,5,NULL,0,0,0,NULL,NULL,NULL,'',1,0),
(35,'Sauvegarde','sauvegarde','Logiciels de sauvegarde','#333',NULL,NULL,11,4,NULL,0,0,0,NULL,NULL,NULL,'',1,0),
(36,'WSL','wsl','Spécifique à Ubuntu pour Windows','#333',NULL,NULL,5,5,NULL,0,0,0,NULL,NULL,NULL,'',1,0),
(37,'Ubuntu Touch','ubuntu-touch','Version d\'Ubuntu dédiée aux appareils mobiles. Attention, il s\'agit encore d\'un projet expérimental.','#333',NULL,NULL,6,5,NULL,0,0,0,NULL,NULL,NULL,'',1,0),
(38,'Partage','partage','','#E95420',NULL,NULL,5,NULL,NULL,0,0,0,NULL,NULL,NULL,'',1,0),
(39,'Événement','evenement','Install Parties, Ubuntu Party, RMLL, festivals du libres, réunions Ubuntu,organisation de toutes les manifestations où vous pensez que Ubuntu a sa place :)','#333',NULL,NULL,0,38,NULL,0,0,0,NULL,NULL,NULL,'',1,0),
(40,'Vos développements libres','vos-developpements-libres','Vous avez un projet logiciel qui vous tient à cœur ? Vous voulez aider et offrir vos compétences ? Ici, la communauté se rassemble pour \"faire\" du libre.','#333',NULL,NULL,1,38,NULL,0,0,0,NULL,NULL,NULL,'',1,0);
/*!40000 ALTER TABLE `flarum_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flarum_users`
--

DROP TABLE IF EXISTS `flarum_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flarum_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nickname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_email_confirmed` tinyint(1) NOT NULL DEFAULT 0,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar_url` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `preferences` blob DEFAULT NULL,
  `joined_at` datetime DEFAULT NULL,
  `last_seen_at` datetime DEFAULT NULL,
  `marked_all_as_read_at` datetime DEFAULT NULL,
  `read_notifications_at` datetime DEFAULT NULL,
  `discussion_count` int(10) unsigned NOT NULL DEFAULT 0,
  `comment_count` int(10) unsigned NOT NULL DEFAULT 0,
  `read_flags_at` datetime DEFAULT NULL,
  `suspended_until` datetime DEFAULT NULL,
  `suspend_reason` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `suspend_message` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `best_answer_count` int(10) unsigned DEFAULT NULL,
  `votes` int(11) NOT NULL,
  `rank` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_vote_time` datetime DEFAULT NULL,
  `bio` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `migratetoflarum_old_password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `flarum_users_username_unique` (`username`),
  UNIQUE KEY `flarum_users_email_unique` (`email`),
  KEY `flarum_users_joined_at_index` (`joined_at`),
  KEY `flarum_users_last_seen_at_index` (`last_seen_at`),
  KEY `flarum_users_discussion_count_index` (`discussion_count`),
  KEY `flarum_users_comment_count_index` (`comment_count`),
  KEY `flarum_users_nickname_index` (`nickname`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flarum_users`
--

LOCK TABLES `flarum_users` WRITE;
/*!40000 ALTER TABLE `flarum_users` DISABLE KEYS */;
INSERT INTO `flarum_users` VALUES
(1,'Admin','Admin','flarum@flarum.docker',1,'$2y$10$YYozzNR53AXySPnDPpy9fu/inobtnNAfAm2usv9xI0yjuA/SlxRlq',NULL,NULL,'2022-07-28 03:41:50','2022-08-11 17:25:57',NULL,'2022-07-28 07:03:10',0,0,'2022-07-28 07:03:08',NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL),
(2,'AdminWiki',NULL,'adminwiki@ubuntu-fr.org',1,'$2y$10$yVP0urMESrrcEjukQFSKSu.xcO7nFKaR6HfFj1FConX9pa2Nt5bDy',NULL,NULL,'2022-08-02 08:41:09','2022-08-02 08:41:09',NULL,NULL,0,0,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL),
(3,'ExpertWiki',NULL,'expertwiki@ubuntu-fr.org',1,'$2y$10$DgFkU3lH5JY43yJnC7XyTugwweqbACuHTlyTDV1XUc71ObjX4QoR6',NULL,NULL,'2022-08-02 08:42:36','2022-08-02 08:42:36',NULL,NULL,0,0,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL),
(4,'User',NULL,'user@ubuntu-fr.org',1,'$2y$10$RsQRzKR/6ncRhNVt6OkAYuUS2fNQ7yqicYeiT0W8rF57LA5B/imky',NULL,NULL,'2022-08-02 08:43:36','2022-08-02 08:43:36',NULL,NULL,0,0,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL),
(5,'Modo',NULL,'modo@ubuntu-fr.org',1,'$2y$10$QyfhEA9tY/6e6DNcZq6u9uOCbLEkXQcbsEW7bg1PUs0Dh5NXLRUgu',NULL,NULL,'2022-08-02 08:45:35','2022-08-02 08:45:35',NULL,NULL,0,0,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `flarum_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flarum_users_notes`
--

DROP TABLE IF EXISTS `flarum_users_notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flarum_users_notes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `note` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `added_by_user_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `flarum_users_notes_added_by_user_id_foreign` (`added_by_user_id`),
  CONSTRAINT `flarum_users_notes_added_by_user_id_foreign` FOREIGN KEY (`added_by_user_id`) REFERENCES `flarum_users` (`id`) ON DELETE SET NULL,
  CONSTRAINT `flarum_users_notes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `flarum_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flarum_users_notes`
--

LOCK TABLES `flarum_users_notes` WRITE;
/*!40000 ALTER TABLE `flarum_users_notes` DISABLE KEYS */;
/*!40000 ALTER TABLE `flarum_users_notes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flarum_warnings`
--

DROP TABLE IF EXISTS `flarum_warnings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flarum_warnings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `strikes` int(11) NOT NULL,
  `public_comment` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `private_comment` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_user_id` int(10) unsigned DEFAULT NULL,
  `hidden_at` datetime DEFAULT NULL,
  `hidden_user_id` int(10) unsigned DEFAULT NULL,
  `post_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `flarum_warnings_created_user_id_foreign` (`created_user_id`),
  KEY `flarum_warnings_hidden_user_id_foreign` (`hidden_user_id`),
  KEY `flarum_warnings_post_id_foreign` (`post_id`),
  CONSTRAINT `flarum_warnings_created_user_id_foreign` FOREIGN KEY (`created_user_id`) REFERENCES `flarum_users` (`id`) ON DELETE SET NULL,
  CONSTRAINT `flarum_warnings_hidden_user_id_foreign` FOREIGN KEY (`hidden_user_id`) REFERENCES `flarum_users` (`id`) ON DELETE SET NULL,
  CONSTRAINT `flarum_warnings_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `flarum_posts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `flarum_warnings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `flarum_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flarum_warnings`
--

LOCK TABLES `flarum_warnings` WRITE;
/*!40000 ALTER TABLE `flarum_warnings` DISABLE KEYS */;
/*!40000 ALTER TABLE `flarum_warnings` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-08-11 19:26:41
