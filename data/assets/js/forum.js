function positionScrubber() {
  if (document.querySelector('div.Scrubber')) {
    let heroRect = document.querySelector('header.Hero').getBoundingClientRect()
    let headerRect = document.querySelector('header#header').getBoundingClientRect()
    let navTop = Math.max(heroRect.bottom, headerRect.bottom)
    document.querySelector('.DiscussionPage-nav>ul').style.top = navTop+'px'
    let scrubberRect = document.querySelector('div.Scrubber').getBoundingClientRect()
    let scrubberHeight = window.innerHeight - scrubberRect.top - 85
    document.querySelector('.Scrubber-scrollbar').style.height = scrubberHeight+'px'
    document.querySelector('.Scrubber-scrollbar').style.maxHeight = scrubberHeight+'px'
  }
}

document.addEventListener('DOMContentLoaded', () => {
  document.querySelector('meta[name=theme-color]').setAttribute("content", "#5E2750")
  positionScrubber()
})

document.addEventListener("scroll", () => {
  if (document.scrollingElement.scrollTop) {
    document.querySelector('#app').classList.add('scrolled')
  }
  else {
    document.querySelector('#app').classList.remove('scrolled')
    setTimeout(() => {
      document.querySelector('#app').classList.remove('scrolled') // dirty hack
    }, 1)
  }
  positionScrubber()
})

window.addEventListener('resize', () => {
  positionScrubber()
})

let observer = new MutationObserver(() => {
  positionScrubber()
})
observer.observe(document.querySelector('#app'), {
  childList: true,
  subtree: true
})
