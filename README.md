# Instance et thème Flarum pour [forum.ubuntu-fr.org](https://forum.ubuntu-fr.org/)

## Développement

- Le thème dépend de [ufr-cms](https://gitlab.com/ubuntu-fr/code/ufr-cms), qui inclut des assets généraux (images, CSS, JavaScript) nécessaires à la bonne présentation du forum.  

### Environnement

- L'environnement de développement peut se déployer en quelques clics depuis [ufr-dev-env](https://gitlab.com/ubuntu-fr/code/ufr-dev-env) (répertoire du thème : `ufr-forum/data/assets`).

### Personnalisation Ubuntu-fr

#### Dans _Administration_ -> _Apparence_ :

##### Couleurs :
- #E95420
- #4b4443

##### Favicon :
https://ubuntu-fr.gitlab.io/code/ufr-main-layout/favicon.png

##### Personnalisation de l’en-tête :
```
<link rel="stylesheet" href="//ufr-cms.localhost/css/main.css">
<link rel="stylesheet" href="/assets/css/forum.css">
<link rel="stylesheet" href="//ufr-cms.localhost/css/print.css" media="print">
<script type="module" src="//ufr-cms.localhost/js/app.js"></script>
<script type="module" src="/assets/js/forum.js"></script>

<header>
  <div class="topbar">
    <a href="/" id="logo">
      <img src="//ufr-cms.localhost/img/logo.svg" alt="ubuntu-fr">
    </a>
    <div id="sandwich">
      <div></div>
      <div></div>
      <div></div>
    </div>
  </div>
  <div class="navbar">
    <nav>
      <a href="//ufr-cms.localhost">Accueil</a>
      <a href="//ufr-doc.localhost">Documentation</a>
      <a href="/" class="active">Forum</a>
      <a href="//ufr-cms.localhost/about">À propos</a>
    </nav>
    <div class="switchers">
      <div class="switcher" id="switcher-wide"><div></div><div></div></div>
      <div href="#" class="switcher" id="switcher-light"></div>
      <div href="#" class="switcher" id="switcher-dark"></div>
    </div>
  </div>
</header>
```

### Guide

La mise en forme générale est à établir sur [ufr-cms](https://gitlab.com/ubuntu-fr/code/ufr-cms).  
Seuls les styles et directives spécifiques à Flarum (éléments de l'interface, styles sur des classes propres à Flarum) sont à rédiger dans les fichiers `data/assets/css/forum.css` et `data/assets/js/forum.js`.
