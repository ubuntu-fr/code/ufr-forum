color_method = \033[93m
containers = flarum_db flarum
app_name = ufr-forum

include .env
export

help: ## Affiche ce message d'aide
	@printf "\n$(color_method)help\033[0m\n\n"
	@awk -F ':|##' '/^[^\t].+?:.*?##/ {\
		printf "\033[36m%-30s\033[0m %s\n", $$1, $$NF \
	}' $(MAKEFILE_LIST) | sort
	@printf "\n"

start: ## Démarre l'application
	@if [ -z "$(shell docker network ls -q -f name=web)" ]; then \
		echo "web network does not exist, creating..."; \
		docker network create web; \
	fi;
	@if [ -z "$(shell docker ps -qa -f name=traefik)" ]; then \
		echo "traefik does not exist, creating..."; \
		docker-compose up -d traefik; \
	elif [ -z "$(shell docker ps -q -f name=traefik)" ]; then \
		echo "traefik is stopped, starting..."; \
		docker start traefik; \
	else \
		echo "traefik is already running, skipping."; \
	fi;
	@if [ -z "$(shell docker ps -qa -f name=mailhog)" ]; then \
		echo "mailhog does not exist, creating..."; \
		docker-compose up -d mailhog; \
	elif 	[ -z "$(shell docker ps -q -f name=mailhog)" ]; then \
		echo "mailhog is stopped, starting..."; \
		docker start mailhog; \
	else \
		echo "mailhog is already running, skipping."; \
	fi;
	@echo "starting ${app_name} containers..."
	@docker-compose up -d $(containers)
	@echo "${app_name} started, made available at http://${app_name}.localhost/"

stop: ## Arrête l'application
	docker-compose stop

logs: ## Affiche le journal en temps réel
	docker-compose logs -f

update: ## Met à jour les containers locaux
	docker-compose build --no-cache
	docker-compose pull

pull: ## Récupère les modifications poussées en production
	git pull

clean: stop ## Supprime les containers de l'application
	docker container rm -f $(containers)

extensions: ## Installe les extensions
	docker-compose exec flarum extension require askvortsov/flarum-moderator-warnings
	docker-compose exec flarum extension require blomstra/search
	docker-compose exec flarum extension require flarum-lang/french
	docker-compose exec flarum extension require flarum/nicknames:1.3
	docker-compose exec flarum extension require fof/ban-ips
	docker-compose exec flarum extension require fof/best-answer
	docker-compose exec flarum extension require fof/drafts
	docker-compose exec flarum extension require fof/filter
	docker-compose exec flarum extension require fof/follow-tags
	docker-compose exec flarum extension require fof/formatting
	docker-compose exec flarum extension require fof/gamification
	docker-compose exec flarum extension require fof/impersonate
	docker-compose exec flarum extension require fof/links
	docker-compose exec flarum extension require fof/merge-discussions
	docker-compose exec flarum extension require fof/moderator-notes
	docker-compose exec flarum extension require fof/pages
	docker-compose exec flarum extension require fof/prevent-necrobumping
	docker-compose exec flarum extension require fof/recaptcha
	docker-compose exec flarum extension require fof/spamblock
	docker-compose exec flarum extension require fof/split
	docker-compose exec flarum extension require fof/stopforumspam
	docker-compose exec flarum extension require fof/upload
	docker-compose exec flarum extension require fof/user-bio
	docker-compose exec flarum extension require fof/user-directory
	docker-compose exec flarum extension require michaelbelgium/flarum-discussion-views
	docker-compose exec flarum extension require migratetoflarum/old-passwords

export: ## Exporte la base données
	docker-compose exec db mysqldump -p${MYSQL_PASSWORD} ${MYSQL_DATABASE} > mariadb/data.sql

import: ## Importe la base de données
	docker exec -i db mysql -p${MYSQL_PASSWORD} ${MYSQL_DATABASE} < mariadb/fixture.sql
